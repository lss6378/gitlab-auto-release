gitlab-auto-release package
========================

Submodules
----------

gitlab-auto-release.cli module
---------------------------

.. automodule:: gitlab-auto-release.cli
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: gitlab-auto-release
    :members:
    :undoc-members:
    :show-inheritance:
